## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
Pokemon catcher

### Short Description
Pokemon catcher is asking from a pokemon api: https://pokeapi.co/api/v2/pokemon/  
for a pokemon which is random number between 1 and 898.  
After this it checks from an internal python server (http://127.0.0.1:5000)  
that does the server have this pokemon already. If not then sends the information of the pokemon to the python server.  
If it does then exits the program.

Information which is saved: Pokemon name, Pokemons types, Pokemons sprite gotten from the pokeapi and meta as string "pokemon".  

### Install
Needed:  
gcc version 11.1.0  
cmake version 3.10  
nlohmann_json  
restclient-cpp  


### Usage
While in root folder  
build it with cmake  
$ make  
$ ./main

### Maintainer(s)

Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### Contributing

Tuomas Uusi-Luomalahti @slupato  
Antti Lehtosalo @mediator246  

### License

Free to use