#include <iostream>
#include <random>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;

// Constant url used to generate catching url
const std::string url = "https://pokeapi.co/api/v2/pokemon/";

// Some variables
std::string newpoketype0, newpoketype1;
std::string catchurl;
std::string newpoke;
std::string pokesprite;

// Functions
int get_random_number();
void catch_pokemon();
int postpokemon(std::string name, std::string type0, std::string type1, std::string default_sprite);
void caught_pokemon();


/* .txt write test funciton
void write();*/



int main(int, char**) {

    // Call catch pokemon function
    catch_pokemon();
    // Call function to check if pokemon has been already caught
    caught_pokemon();
    // Save pokemon to server 
    postpokemon(newpoke, newpoketype0, newpoketype1, pokesprite);

    
    std::cout << "You caught: " << newpoke << std::endl;
    std::cout << "Type1: " << newpoketype0 << std::endl << "Type2: " << newpoketype1 << std::endl;
    std::cout << "Pokemon image: " << pokesprite << std::endl;
    

}


int get_random_number()
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist898(1,898);

    int random = dist898(rng);
    return random;
};

void catch_pokemon()
{
    std::cout << "Catching a pokemon!\n";

    // Generate random number and save it in variable
    int num = get_random_number();
    // Convert generated random number to string
    std::string pokenum = std::to_string(num);
    // Create pokemon catching url by combining constant url and random number
    catchurl = url + pokenum;
  
    // Get data from generated url
    RestClient::Response r = RestClient::get(
    catchurl);

    // Parse url data
    auto json_body = json::parse(r.body);

    // Find pokemon name and save in variable
    newpoke = json_body["name"];
    
    // Find pokemon types and save in variables
    auto types_json = json_body["types"];
    for (int i = 0; i < types_json.size(); i++) {
        if (i == 0)
        {
            newpoketype0 = types_json[i]["type"]["name"];
        } else if (i == 1)
        {
            newpoketype1 = types_json[i]["type"]["name"];
        }
        //std::cout << types_json[i]["type"]["name"] << std::endl;
    }

    // Find link to pokemon picture and save in variable
    auto sprite_json = json_body["sprites"];
    pokesprite = sprite_json["front_default"];
};


void caught_pokemon()
{
    // Get data from python server
    RestClient::Response alldata = RestClient::get("http://127.0.0.1:5000/fetch/all");
    auto json_data = json::parse(alldata.body);

    // Check if pokemon has been caught already
    for (int i = 0;i < json_data.size(); i++)
    {
        std::string pokemon = json_data[i]["data"];
        json json_pkmn = json::parse(pokemon);
        if (newpoke == json_pkmn["name"])
        {   
            std::cout << "Oops, you already have that one!" << std::endl;
            exit (EXIT_SUCCESS);
        }
    }

    /*std::string pokemon = json_data[0]["data"];
    json json_pkmn = json::parse(pokemon);
    std::cout << json_pkmn["name"] << std::endl;*/

}


int postpokemon(std::string name, std::string type0, std::string type1, std::string default_sprite)
{   
    /* Type variables
    json poke_types;
    poke_types [0] = type0;
    poke_types [1] = type1;*/

    // Pokemon data
    json json_data;
    json_data ["name"] = name;
    json_data ["type 1"] = type0;
    json_data ["type 2"] = type1;
    json_data ["default_sprite"] = default_sprite;

    // Pokemon payload to send
    json message;
    message["data"] = json_data.dump();
    message["meta"] = "pokemon";


    // initialize RestClient
    RestClient::init();

    // get a connection object
    RestClient::Connection* conn = new RestClient::Connection("http://127.0.0.1:5000/");

    // set header to json
    RestClient::HeaderFields headers;
    headers["Accept"] = "application/json";
    conn->SetHeaders(headers);

    // then send a POST request with a payload
    conn->AppendHeader("Content-Type","application/json");
    RestClient::Response sendpoke = conn->post("/submit", message.dump());

    // Close connection
    RestClient::disable();
    return 0;
}

/*void write()
{
    std::fstream write("/home/slupato/pokemons.txt", std::ios::out | std::ios::app);

    write << "Pokemon: " << newpoke << std::endl;
    write << "Type(s): " << newpoketype0 << " " << newpoketype1 << std::endl;
    write << "Picture: " << pokesprite << std::endl << std::endl;

    write.close();
}*/
